# Das Terminal, Nützlich im Alltag oder nur für Hacker?

Das Linux-Terminal kann im Alltag eine große Hilfe sein, sogar wenn man nicht Linux verwendet. In diesem Workshop gibt es dazu eine Einführung, u.A. lernt ihr das hier kennen:

- Einführung zu Bash (cd, ls, cat,…)
- Nützliche Befehle/Features (nano, man, pipes…)
- Lustige Befehle (sl,lolcat,toilet,edbrowse,cmatrix,…)
